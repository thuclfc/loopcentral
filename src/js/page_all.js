$(document).ready(function () {
    $('.navbar-toggler').click(function () {
        $('header nav').toggleClass('active');
        $('.modal-menu').addClass('is-open');
    });
    $('.navbar-collapse .close,.modal-menu').click(function(){
        $('.navbar').removeClass('active');
        $('.modal-menu').removeClass('is-open');
    });

    // active navbar of page current
    var urlcurrent = window.location.href;
    $(".navbar-nav li a[href$='"+urlcurrent+"']").addClass('active');

    $(window).on("load", function (e) {
        $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'><i class='fas fa-sort-down'></i></span>");
        $('.loading').hide();
    });
    $('.navbar-nav > li').click(function () {
        $(this).toggleClass('active');
    });

    // effect navbar
    $(window).scroll(function () {
        if ($(this).scrollTop() > 210) {
            $('header,main,.page-detail .social').addClass('scroll');
        }else{
            $('header,main,.page-detail .social').removeClass('scroll');
        }
    });

    $('.btn_close').on('click',function (){
       $('.search').removeClass('show').addClass('hide');
    });
    $('.btn_search').on('click',function (){
        $('.search').removeClass('hide').addClass('show');
        $('.search input').focus();
    });
    document.addEventListener('keydown', function(event) {
        const key = event.key; // const {key} = event; in ES6+
        if (key === "Escape") {
            $('.search').removeClass('show').addClass('hide');
        }
    });

    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight() - 100;

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height() - 100;

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };
    $(window).on('resize scroll load', function() {
        $('.fadeup').each(function() {
            if ($(this).isInViewport()) {
                $(this).addClass('fadeInUp').css({'opacity':'1','visibility':'visible'});
            }
        });
        $('.fadein').each(function() {
            if ($(this).isInViewport()) {
                $(this).addClass('fadeIn').css({'opacity':'1','visibility':'visible'});
            }
        });
        $('.zoomin').each(function() {
            if ($(this).isInViewport()) {
                $(this).addClass('zoomIn').css({'opacity':'1','visibility':'visible'});
            }
        });
    });
});
